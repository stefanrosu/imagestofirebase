# Upload image to Firebase using Angularfire2 (AngularFireModule).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## What you need to INSTALL!

1.Install Angular CLI

2.Install node_modules `npm install`

3.Install bootstrap `npm insall bootstrap` and add on style.scss `@import '~bootstrap/dist/css/bootstrap.min.css';`

4.Install Firebase and Angularfire2 `sudo npm i firebase angularfire2`

5.Check app.modules.ts and import the AngularFireModule `import { AngularFireModule } from 'angularfire2';` and AngularFireStorageModule `import { AngularFireStorageModule } from 'angularfire2/storage';` also on imports array. Note: the `storageBucket` link is the url folder from firebase -> files and top of panel (copy from button)

6.You can check the the tutorial of firebase setup and stuff from this link: [Firebase Storage Tutorial](https://www.youtube.com/watch?v=C1xb_LsU31M&t=1303s)

7.If you want to downloadSrc from firebase after you uploaded a file you need to change from video the last part: `this.downloadURL = this.task.downloadURL();` into ` this.downloadURL = this.ref.getDownloadURL();`

8.To GET data from firebase you need to install `npm install firebase angularfire2 --save` and import in `app.module.ts` the following: `import { AngularFireDatabase } from 'angularfire2/database';`, `providers: [AngularFireDatabase],` and be sure you have set the `databaseURL: "https://upload-images-project-7b040.firebaseio.com",` on the initializeApp. Also, you must import `import { AngularFireDatabase } from 'angularfire2/database';` and in constructor `constructor(private firebase: AngularFireDatabase) { }` on each page where you want to use it.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
