import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  public booksData;

constructor(private firebase: AngularFireDatabase) { }

  ngOnInit() {
    this.getAllBooks();
  }

  getAllBooks(){
    this.firebase.list('books').snapshotChanges().subscribe(books => {
      this.booksData = books.map(item => {
        return {
          $key: item.key,
          ...item.payload.val()
        };
      });
    },err => console.log('err', err));
  }
}
