import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  selectedFile: File = null;
  successMessage: string = '';
  formatMessage: string = '';
  fileInputName: string = '';
  public disabledUploadBtn: boolean = true;


  constructor(private afStorage: AngularFireStorage) { }

  ngOnInit() {
  }


  displayText(imageName: string, formatImage: string, uploadedImage: string){
    this.fileInputName = imageName;
    this.formatMessage = formatImage;
    this.successMessage = uploadedImage;
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
    this.fileInputName = this.selectedFile.name;
    this.displayText(this.fileInputName, '', '');

    const regex = new RegExp('(.*?)\.(jpg|jpeg|png)$');
    const checkImageFormat = regex.test(this.fileInputName.toLowerCase())

    if (checkImageFormat) {
      this.disabledUploadBtn = false;
    } else if (!checkImageFormat){
        this.disabledUploadBtn = true;
        this.displayText('', 'Please check if your format file is png, jpg or jpeg, thank you!', '');
    }

  }

  upload() {
    this.ref = this.afStorage.ref(this.selectedFile.name);
    this.task = this.ref.put(this.selectedFile);
    this.uploadProgress = this.task.percentageChanges();
    this.downloadURL = this.ref.getDownloadURL();
    this.displayText('', '', 'Your file was uploaded!');
  }

}
